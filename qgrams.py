import dinopy
import argparse
import random
import os.path

"""This is the python file of the BioInfarmatics project of TU Dortmund.

It is able to read and write fasta files. Also it generates a random sequence with
a given length and checks for its q-uniqueness.

written by Nico Bertram
reviewed by Patrick Schmelter
last edited 27.05.2016
"""

#for furhter use with bitbucket uncomment the first following line, not the second!
#__version__ = "$Revision$"
# $Source$

bases = ["A", "C", "G", "T"]

def generate(length, a=0, c=0, g=0, t=0):
    """ Von Shimon geklaut """
    if (a+c+g+t) < 0.999 or (a+c+g+t) > 1.001:
        return "".join([random.choice(bases) for i in range(0,length)])
    else:
        list = []
        for i in range(0, length):
            if i / length < a:
                list.append('A')
            elif i / length < a + c:
                list.append('C')
            elif i / length < a + c + g:
                list.append('G')
            else:
                list.append('T')
        random.shuffle(list)
        return "".join(list)
        
def get_result(dna, q, is_fasta):
    """ Von Shimon geklaut """
    if is_fasta:
        dna_sequence = dna.genome().sequence.decode("utf-8")
        file_info = dna.genome().info[0].name.decode("utf-8") + "\n"
    else:
        dna_sequence = dna
        file_info = ">This is a random generated sequence\n"
    qgrams = dinopy.qgrams(dna_sequence, q)
    first_sub = first_q_unique_subsequence_shimon(dna_sequence, q) + "\n"
    qgram_string = '\n'.join(qgrams)
    return file_info + first_sub + qgram_string
    
def first_q_unique_subsequence_shimon(dna_sequence, q):
    forbidden_qgrams = set()
    for i in range(0, len(dna_sequence) - q + 1):
        qgram = dna_sequence[i:i+q]
        rev = reverse_complement(qgram)
        if qgram in forbidden_qgrams or qgram == rev:
            return dna_sequence[0:i+q-1]
        else:
            forbidden_qgrams.add(qgram)
            forbidden_qgrams.add(rev)
    return dna_sequence

def _write_fasta(filename, entry):
    """Write an entry into a file and overwrite it if necessary."""
    with dinopy.FastaWriter(filename, force_overwrite=True) as faw:
        faw.write_entry(entry[0], dtype=str)

def _read_fasta(filename):
    """Reads the first entry from the given file."""
    _reader = dinopy.FastaReader(filename)
    _generator = _reader.entries(str)
    return _generator

def _write_qgrams(filename, info, sequence, q):
    """This method writes already processed qrams into a fasta-file."""
    if not os.path.exists(filename):
        _file = open(filename, 'w')
    else:
        _file = open(filename, 'a')
    _file.write('>' + info + '\n')
    for _qgram in dinopy.qgrams(sequence, q):
        _file.write(_qgram + '\n')
    _file.close()
    
def _create_sequence_entry(name, sequence):
    """Use this method to put a name and a sequence into one object."""
    _sequence_entry = (sequence, name.encode())
    return [_sequence_entry]
    
def generate1(length):
    """Generates a random sequence based on ATCG with a given length."""
    _bases = "ATCG"
    _sequence = ""
    for _i in range(0, length):
        _rand = random.randrange(4)
        _sequence += _bases[_rand]
    return _sequence
    
def check_quniqueness(sequence, q):
    """Checks if a sequence is q-unique for a given q."""
    _qGrams = qGram_decompose(sequence, q)
    _values = _qGrams.values()
    for _value in _values:
        if _value != 1:
            return False
    for _qGram in _qGrams:
        if rev_comp(_qGram) in _qGrams:
            return False
    return True      

def qgram_counter(sequence, q):
    """Returns a dictionary with the qgrams and the number of their occurances in sequence with given size q"""
    number_of_qgrams = len(sequence) - q + 1
    qgrams = [sequence[position:position+q] for
              position in range(number_of_qgrams)]

    qgram_count = {}
    for qgram in qgrams:
        if qgram in qgram_count:
            qgram_count[qgram] = qgram_count[qgram] + 1
        else:
            qgram_count[qgram] = 1
    return qgram_count
	
def print_qgramsCount(sequence, q):
    """Prints a list of the qgrams and the number of their occurances in given sequence with size q"""
    print("qgrams and the number of their occurances:")
    for qgram, count in qgram_counter(sequence, q).items():
        print(qgram, ':', count)

def first_qunique_subsequence(sequence, q):
    """Returns the last position of the first qunique subsequence.
    """
    found_qgrams = set()

    position = 0
    number_of_qgrams = len(sequence) - q + 1
    for position in range(number_of_qgrams):
        qgram = sequence[position:position+q]
        if qgram in found_qgrams or \
                dinopy.reverse_complement(qgram) in found_qgrams:
            return position+q
        else:
            found_qgrams.add(qgram)
            position += 1
    return number_of_qgrams + q

def print_first_qUnique_subsequence(sequence, q):
    """Prints the longest prefix of the sequence which is qunique with given q"""
    subseq_index = first_qunique_subsequence(sequence, q)
    print("First qunique subsequence:")
    print(sequence[:subseq_index-1])
	
def qGram_decompose(seq, q):
    """The sequence seq is decomposed in qgrams with given size q"""
    qGrams = {}
    for i in range(q-1,len(seq)):
        if seq[i-q+1:i+1] in qGrams:
            value = qGrams[seq[i-q+1:i+1]]
            value += 1
            qGrams.update({seq[i-q+1:i+1] : value})
        else:
            qGrams.update({seq[i-q+1:i+1] : 1})
    return qGrams
    
def reverse_complement(seq):
    """Returns the reverse complement of sequence seq"""
    rev = ''
    n = len(seq)
    for i in range(0, n):
        if seq[n-i-1] == 'A':
            rev = rev + 'T'
        elif seq[n-i-1] == 'C':
            rev = rev + 'G'
        elif seq[n-i-1] == 'G':
            rev = rev + 'C'
        else:
            rev = rev + 'A'
    return rev