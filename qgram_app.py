import argparse
import qgrams



def __main__():
    """This is the main-method. It handels argparse and starts the program for different inputs."""
    _arg_parser = argparse.ArgumentParser(
        description="Reads a sequence from a Fasta-File and writes all q-grams in a txt-File")
    _group = _arg_parser.add_mutually_exclusive_group(required=True)
    _group.add_argument("-s", "--sequence",
                       help="The sequence from which the qgrams are generated")
    _group.add_argument("-f", "--input-file",
                       help="File from which the sequence is read")
    _group.add_argument("-r", "--random", type=int,
                       help="Create a random sequence with given size")
    _arg_parser.add_argument("q",
                            help="The size of the q-grams", type=int)
    _arg_parser.add_argument("-w", "--write", 
                            help="File to which the q-grams are saved")
    _cmd_args = _arg_parser.parse_args()

    # parse q
    _q = _cmd_args.q

    # parse the sequence or read it from file
    if _cmd_args.sequence:
        _sequences = qgrams._create_sequence_entry(name="Sequence from command line.", sequence=_cmd_args.sequence)
    elif _cmd_args.input_file:
        _sequences = qgrams._read_fasta(_cmd_args.input_file)
    # generate random sequence
    elif _cmd_args.random:
        _sequences = qgrams._create_sequence_entry(name="Random sequence.", sequence=qgrams.generate(_cmd_args.random))
        qgrams._write_fasta("test.fasta", _sequences)
        
    for _entry in _sequences:
        _name = _entry[1]
        print(_name)
        # if the argument write is given, write qgrams to file
        if _cmd_args.write:
            qgrams._write_qgrams(_cmd_args.write, _name, _entry.sequence, _q)

__main__()