from spyre import server
import qgrams as q
import pandas as pd
import dinopy

class QGramApp(server.App):
    title = "q-gram application"
    inputs = [{"type":"text",
               "key":"sequence",
               "label":"Enter a sequence",
               "value":"GATTACA"},
               {"type":"text",
               "key":"q",
               "label":"Enter a q",
               "value":"3"},
               {"type":"text",
                "key":"rand_length",
                "label":"Enter the size of a random sequence",
                "value":"10"},
                {"type":"text",
                "key":"a_freq",
                "label":"Enter the frequency of A",
                "value":"0.25"},
                {"type":"text",
                "key":"t_freq",
                "label":"Enter the frequency of T",
                "value":"0.25"},
                {"type":"text",
                "key":"c_freq",
                "label":"Enter the frequency of C",
                "value":"0.25",},
                {"type":"text",
                "key":"g_freq",
                "label":"Enter the frequency",
                "value":"0.25"},
                {"type":"text",
                "key":"file",
                "label":"Enter a file path",
                "value":""},
                {"type":"dropdown",
                 "label":"Choose method",
                 "options":[{"label":"Random Sequence", "value":"Rand"},
                            {"label":"File","value":"file"},
                            {"label":"Enter Sequence", "value":"Enter"}],
                 "key":"choose"}]
                
    controls=[{"type":"button",
               "label":"Generate",
               "id":"gen"}]
    tabs=["Informations", "QGrams"]
    outputs=[{"type":"html",
              "id":"info",
              "control_id":"gen",
              "tab":"Informations"},
             {"type":"table",
              "id":"qgram_table",
              "tab":"QGrams",
              "control_id":"gen",
              "on_page_load":True}]
              
    def __init__(self):
        self.rand_seq = ""
        self.file_seq = ""
              
    def getHTML(self, params):
        if params["choose"] == "Rand":
            length = int(params["rand_length"])
            a = float(params["a_freq"])
            g = float(params["g_freq"])
            c = float(params["c_freq"])
            t = float(params["t_freq"])
            self.rand_seq = q.generate(length, a, c, g, t)
            return self.rand_seq
        if params["choose"] == "Enter":
            return params["sequence"]
        if params["choose"] == "file":
            dna = dinopy.FastaReader(params["file"])
            self.dna_seq = dna.genome().sequence.decode("utf-8")
            return q.get_result(dna, int(params["q"]), True)
            
    def getData(self, params):
        print(self.rand_seq)
        if params["choose"] == "Enter":
            qgram_counter = q.qgram_counter(params["sequence"], int(params["q"]))
        if params["choose"] == "Rand":
            str=self.rand_seq
            qgram_counter = q.qgram_counter(str, int(params["q"]))
        if params["choose"] == "file":
            qgram_counter = q.qgram_counter(self.dna_seq, int(params["q"]))
        #keys = list(qgram_counter.keys())
        #values = list(qgram_counter.values())
        return pd.DataFrame(data=list(qgram_counter.items()), columns=["QGram", "Number of Occurences"])
      
app = QGramApp()
app.launch()     
               
               
